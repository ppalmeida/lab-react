Alguma view PHP:




.....




<?php foreach ($sections as $s => $section): ?>
<section class="container" data-section="{{$section->id}}">
    <div class="row">
        <ul id="items-for-section-{{$section->id}}">
            <?php foreach($section->menu as $i => $menuItem): ?>
                <li
                  data-selected="{{$menuItem->section()->id === $section->id ? '1' : '0'}}"
                  data-item="{{$menuItem->id}}"
                  data-show-webview="{{($i % 2 ? 1 : 0)}}"
                >{{$menuItem->name}}</li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>
<?php endforeach; ?>




.......

































// Algum Script JS
<script>
$('li["data-section]').click(function(evt) {
    evt.preventDefault();
    var currentItem = $(evt.target);
    var section = $(this).parent().parent().data.section;
    var ul = $(this).parent();

    $.post('/some-api', {
      item: $(this).data('item'),
      section: section
    })
    .then(function(newMenuItems) {
        $('li["data-show]').attr('data-show', 0);
        currentItem.attr('data-selected', 1);

        for (var i = 0; i < newMenuItems.length; i++) {
          var item = newMenuItems[i];
          var li = '<li data-selected="'section.id === item.section.id'" data-item="'+item.id+'" data-show-webview="' + ((i % 3) ? '1' : '0') + '">'item.name'</li>';
          $(ul).append(li);

          // Agora adicionar o click ao nó criado, mas é string, tem que esperar o DOM, etc...
        }
    });
    .fail(etc..);
});
</script>
