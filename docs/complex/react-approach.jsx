// The main holder...
const MenuHolder = ({ menu, onChange }) (
    <section class="container">
        <div class="row">
            <MyAwesomeMenu menu={menu} onChange={item => onChange(item)} />
        </div>
    </section>
);

export default MenuHolder;


// The MyAwesomeMenu.js file:
class MyAwesomeMenu extends Component {
    constructor(props) {
        super(props);
        this.onItemClicked = this.onItemClicked.bind(this);
    }

    onItemClicked(vo) {
        // execute callback received?
        this.props.onChange(vo);
    }

    renderList(vo) {
        return (
            <li selected={vo.selected} onClick={evt => this.onItemClicked(vo)}>{vo.name}</li>
        );
    }

    render() {
        const lis = menu.map(vo => this.renderList(this.props.menu));
        return (
            <ul>
                {lis}
            </ul>
        );
    }
}
