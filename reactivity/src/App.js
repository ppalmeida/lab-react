import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = { userInput: '' };
  }

  onChange(userInput) {
    this.setState({ userInput });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <section id="reactivity">
            <div className="container">
              <input type="text" onChange={evt => this.onChange(evt.target.value)}/>
              <h3 className="text-result">Você digitou: {this.state.userInput}</h3>
            </div>
        </section>
      </div>
    );
  }
}

export default App;
