import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class AppCondicionalRendering extends Component {

  constructor(props) {
    super(props);

    this.state = { userInput: '' };
  }

  onChange(userInput) {
    this.setState({ userInput });
  }

  render() {
    const { userInput } = this.state;
    const lastCharIsVowel = [' ', 'a', 'e', 'i', 'o', 'u'].indexOf(userInput.toLowerCase().charAt(userInput.length - 1)) > -1;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <section id="reactivity">
            <div className="container">
              <input className={userInput.length === 0 ? '' : lastCharIsVowel ? 'green' : 'red'} type="text" onChange={evt => this.onChange(evt.target.value)}/>
              <h3 className="text-result">Você digitou: {this.state.userInput}</h3>
              {lastCharIsVowel && <input className="btn btn-primary" type="button" value="Enviar!" /> }
            </div>
        </section>
      </div>
    );
  }
}

export default AppCondicionalRendering;
