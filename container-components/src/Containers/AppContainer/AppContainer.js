import React, { Component } from 'react';
import Arizona from '../../Components/ArizonaComponent/ArizonaComponent';
import arizonaLogo from '../../Icons/ArizonaLogo/arizona.png';

class AppContainer extends Component {

  constructor(props) {
    super(props);

    this.state = { logo: true };
    this.onLogoClick = this.onLogoClick.bind(this);
  }

  onLogoClick() {
    const logo = !this.state.logo;
    this.setState({ logo });
  }

  render() {
    const logo = this.state.logo ? arizonaLogo : null;
    return (
      <Arizona logo={logo} onLogoClick={this.onLogoClick} title="React Lab"  />
    );
  }
}

export default AppContainer;