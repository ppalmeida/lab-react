import React from 'react';

import Header from '../Header/Header';
import './App.css';

const ArizonaComponent = ({ onLogoClick, logo = null, title = 'Lab de React' }) => {
  return (
    <div className="App">
      <Header title={title} logo={logo} onLogoClick={onLogoClick} />
      <p className="App-intro">
        To get started, edit <code>src/App.js</code> and save to reload.
      </p>
    </div>
  );
}

export default ArizonaComponent;
