import React from 'react';
import './Header.css';
import logoDefault from './logo.svg';

const Header = ({ onLogoClick, logo = null, title = 'React Rocks!' }) => {
  return (
    <header className="App-header">
      <img src={logo ? logo : logoDefault} className="App-logo" onClick={onLogoClick} alt="logo" />
      <h1 className="App-title">{title}</h1>
    </header>
  );
}

Header.propTypes = {

}

export default Header;
