import React from 'react';
import AppContainer from '../Containers/AppContainer/AppContainer';

const Home = () => {
  return(
    <div>
      <AppContainer />
    </div>
  );
}

export default Home;